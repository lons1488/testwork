package com.sersh.testtaskpecode

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.View.INVISIBLE
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_main.*


class MainFragment : Fragment() {

    private var myId = 0

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.fragment_main, container, false)
    }

    override fun onResume() {
        getBundleArgument()
        text_count.text = myId.toString()
        if (myId == 0) minus.visibility = INVISIBLE
        initListener()

        super.onResume()
    }

    private fun getBundleArgument() {
        val bundle = this.arguments
        this.myId = bundle?.getInt("id", android.R.attr.defaultValue) ?: myId
    }

    private fun initListener() {
        plus.setOnClickListener {
            (activity as MainActivity).addFragment()
        }
        minus.setOnClickListener {
            (activity as MainActivity).removeFragment(myId)
        }
        notification_button.setOnClickListener {
            (activity as MainActivity).createNotification(myId)
        }
    }

}