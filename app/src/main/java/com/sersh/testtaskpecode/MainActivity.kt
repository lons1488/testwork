package com.sersh.testtaskpecode

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.os.Build
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.NotificationCompat
import androidx.fragment.app.Fragment
import androidx.viewpager2.widget.ViewPager2


class MainActivity : AppCompatActivity() {

    private lateinit var adapter: ViewPagerAdapter
    private lateinit var viewPager: ViewPager2
    private var fragmentArray: ArrayList<Fragment> = arrayListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        fragmentArray.add(MainFragment())
        initViewPager()
    }

    private fun initViewPager() {
        adapter = ViewPagerAdapter(this, fragmentArray)
        viewPager = findViewById(R.id.view_pager)
        viewPager.adapter = adapter
    }

    fun addFragment() {
        val bundle = Bundle()
        val fragment = MainFragment()
        fragmentArray.add(fragment)
        bundle.putInt("id", fragmentArray.size - 1)
        fragment.arguments = bundle
        adapter.notifyDataSetChanged()
        viewPager.currentItem = fragmentArray.size
    }

    fun removeFragment(id: Int) {
        if (fragmentArray.size == 1) return
        deleteNotification(fragmentArray.size - 1)
        fragmentArray.removeAt(fragmentArray.size - 1)
        adapter.notifyDataSetChanged()
        if (id == fragmentArray.size)
            viewPager.currentItem = fragmentArray.size - 1

    }

    fun createNotification(id: Int) {
        val channelId = "my_channel_01"
        val notificationManager =
            getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name: CharSequence = "my_channel"
            val importance = NotificationManager.IMPORTANCE_HIGH
            val mChannel = NotificationChannel(channelId, name, importance)
            mChannel.setShowBadge(false)
            notificationManager.createNotificationChannel(mChannel)
        }

        val builder: NotificationCompat.Builder = NotificationCompat.Builder(this, channelId)
            .setSmallIcon(R.drawable.avatar)
            .setContentTitle(resources.getString(R.string.notification_title))
            .setContentText(resources.getString(R.string.notification_description) + id)
        notificationManager.notify(id, builder.build())
    }

    private fun deleteNotification(id: Int) {
        val notificationManager =
            getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.cancel(id)
    }
}