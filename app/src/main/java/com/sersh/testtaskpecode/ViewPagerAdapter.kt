package com.sersh.testtaskpecode

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity

import androidx.viewpager2.adapter.FragmentStateAdapter
import java.util.ArrayList


class ViewPagerAdapter(
    fragment: FragmentActivity,
    private var fragmentArray: ArrayList<Fragment>
) : FragmentStateAdapter(fragment) {

    override fun getItemCount(): Int = fragmentArray.size

    override fun createFragment(position: Int): Fragment {

        return fragmentArray[position]
    }
}